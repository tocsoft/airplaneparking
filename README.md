### How to run
This uses blazor WebAssembly as its UI layer hosted in an aspnetcore web application. 

To run make sure `AirportParking.Server` is configured to be your start up application then just run from visual studio (F5 away).

Alternatively you can run by executing `dotnet run` from `./src/AirportParking.UI/Server/`

Page will be available from https://localhost:5001 by default.


### Notes

Difficulty not having a solid decision on DB architecture. DB/Technology design can have massive impact on how the solution is architected. I landed on a solution that could map to entity framework quite easily and this could be made to work with document databases too.

I decided (due to time constraints) to produce the initial simplified model where a space tracks its own occupancy rather than the more ideal solution where the booking is a sperate entity which would allow for better analysis of usage which could be used to build a more complex model then the current one of just find the first available spot sorted smallest to largest.

Future booking would require knowing how long current bookings are likely to last otherwise you are much more likely double book and I can imagine double booking a plane is a lot worse that a car spot. The way I would approach future booking would be not be to book them into specific slots but book them against the capacity. To decide what space to book a new plane into would require us to know what the capacity will be like what its dues to leave and book it into the smallest space that is available now and will still be available in the future. All then allocation and projection requires having a firm understanding on how long a plane will be sat in spot and as that was not clear then I went for the more naive solution that should only require an extra two arguments (start and end datetime) on the booking and find space 
APIs.

Initially thought I would need all sorts of management infrastructure be decided that was out of scope and management could happen out of band. (Scratch app for resetting and managing seed data).

This was written as web API first and then a UI was build on top of it. Initially this was going to just stop at being a restful API but the I noticed the requirement for an 'obvious alert' made me throw a Blazor UI on top of my API.

