﻿using AirportParking.Server;
using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Terminal.Gui;

namespace AirportParking.Scratch
{

    class Program
    {
        static async Task Main(string[] args)
        {
            var db = new Database.JsonFileParkingSpaceDatabase("../../../../AirportParking.UI/Server/DataDirectory");

            var crafts = await db.QueryAircraftType(c => c);
            foreach (var craft in crafts)
            {
                await db.Remove(craft);
            }
            var spaces = await db.QueryParkingSpaceDBModel(c => c);
            foreach (var space in spaces)
            {
                await db.Remove(space);
            }

            for (var x = 1; x <= 5; x++)
                for (var y = 1; y <= 5; y++)
                {
                    await db.Add(new ParkingSpaceDBModel
                    {
                        Id = Guid.NewGuid(),
                        Location = $"Runway: {x} Space :{y}",
                        Name = $"R{x}C{y}",
                        OccupiedByAircraftType = null,
                        Size = ParkingSpaceSize.Jumbo
                    });
                }

            for (var x = 1; x <= 5; x++)
                for (var y = 1; y <= 10; y++)
                {
                    await db.Add(new ParkingSpaceDBModel
                    {
                        Id = Guid.NewGuid(),
                        Location = $"Runway: {x} Space :{y + 5}",
                        Name = $"R{x}C{y + 5}",
                        OccupiedByAircraftType = null,
                        Size = ParkingSpaceSize.Jet
                    });
                }

            for (var x = 1; x <= 5; x++)
                for (var y = 1; y <= 5; y++)
                {
                    await db.Add(new ParkingSpaceDBModel
                    {
                        Id = Guid.NewGuid(),
                        Location = $"Runway: {x} Space :{y + 15}",
                        Name = $"R{x}C{y + 15}",
                        OccupiedByAircraftType = null,
                        Size = ParkingSpaceSize.Prop
                    });
                }


            await db.Add(new AircraftType
            {
                EligableSizes = ParkingSpaceSize.Jumbo | ParkingSpaceSize.Prop | ParkingSpaceSize.Jet,
                Reference = "E195"
            });

            await db.Add(new AircraftType
            {
                EligableSizes = ParkingSpaceSize.Jumbo | ParkingSpaceSize.Jet,
                Reference = "A330"
            });

            await db.Add(new AircraftType
            {
                EligableSizes = ParkingSpaceSize.Jumbo | ParkingSpaceSize.Jet,
                Reference = "B777"
            });

            await db.Add(new AircraftType
            {
                EligableSizes = ParkingSpaceSize.Jumbo,
                Reference = "A380"
            });

            await db.Add(new AircraftType
            {
                EligableSizes = ParkingSpaceSize.Jumbo,
                Reference = "B747"
            });

            await db.SaveAsync();
        }
    }
}