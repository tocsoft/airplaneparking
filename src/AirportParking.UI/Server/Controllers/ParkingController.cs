﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AirportParking.Server;
using AirportParking.Shared;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AirportParking.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParkingController : ControllerBase
    {
        private readonly ILogger<ParkingController> _logger;
        private readonly IParkingSpaceRepository parkingSpaceRepository;

        public ParkingController(ILogger<ParkingController> logger, IParkingSpaceRepository parkingSpaceRepository)
        {
            _logger = logger;
            this.parkingSpaceRepository = parkingSpaceRepository;
        }

        [HttpGet("Summary")]
        public Task<IEnumerable<ParkingSpaceSizeCapacity>> GetSummaryAsync()
        {
            return parkingSpaceRepository.GetAvailibilitySummaryAsync();
        }

        [HttpGet]
        public Task<IEnumerable<ParkingSpace>> ListSpacesAsync(bool? booked)
        {
            // booked :
            // - null means all,
            // - true only booked,
            // - false only free
            return parkingSpaceRepository.AllSpacesAsync(new BookingFilter
            {
                IncludeBooked = booked != false, // works for null and true
                IncludeFree = booked != true, // works for null and false
            });
        }

        [HttpPost("Book")]
        [ProducesResponseType(400)]
        public async Task<IActionResult> BookSpaceAsync(BookingRequest booking)
        {
            if (await parkingSpaceRepository.TryBookAsync(booking.SpaceId, booking.AircraftType))
            {
                return this.Ok();
            }

            var avil = await parkingSpaceRepository.GetAvailibilityAsync(booking.AircraftType);

            if (avil.IncompatableSpaces.Any(s => s.Id == booking.SpaceId))
            {
                return this.BadRequest("Requested space is not compatable with the requested aircraft");
            }

            if (avil.BookedSpaces.Any(s => s.Id == booking.SpaceId))
            {
                return this.Conflict("Requested space is already booked");
            }

            _logger.LogError("Some other booking error is happening, db level maybe!");
            return this.BadRequest("Unknown error");
        }

        [HttpPost("RemoveBooking")]
        public async Task<IActionResult> RemoveBookingAsync(RemoveBookingRequest booking)
        {
            if (await parkingSpaceRepository.TryRemoveBookingAsync(booking.SpaceId))
            {
                return this.Ok();
            }

            return this.Conflict("Requested space is not booked");
        }

        [HttpPost("FindSpace")]
        public Task<ParkingSpaceAvailibility> FindAllocationOptionsAsync([FromBody] AvailibilityRequest request)
        {
            return parkingSpaceRepository.GetAvailibilityAsync(request.AircraftType);
        }

    }
}
