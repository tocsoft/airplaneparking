﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AirportParking.Server;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AirportParking.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AircraftController : ControllerBase
    {
        private readonly ILogger<AircraftController> _logger;
        private readonly IParkingSpaceRepository parkingSpaceRepository;

        public AircraftController(ILogger<AircraftController> logger, IParkingSpaceRepository parkingSpaceRepository)
        {
            _logger = logger;
            this.parkingSpaceRepository = parkingSpaceRepository;
        }

        [HttpGet]
        public Task<IEnumerable<string>> ListAircraftTypesAsync()
        {
            return parkingSpaceRepository.GetAircraftTypesAsync(); ;
        }
    }
}
