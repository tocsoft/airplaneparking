﻿namespace AirportParking
{
    public struct BookingFilter
    {
        public bool IncludeBooked { get; set; }
        public bool IncludeFree { get; set; }
    }
}
