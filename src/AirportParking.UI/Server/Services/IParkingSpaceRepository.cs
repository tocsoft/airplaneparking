﻿using AirportParking.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AirportParking.Server
{
    public interface IParkingSpaceRepository
    {
        Task<IEnumerable<string>> GetAircraftTypesAsync();
        Task<IEnumerable<ParkingSpaceSizeCapacity>> GetAvailibilitySummaryAsync();
        Task<ParkingSpaceAvailibility> GetAvailibilityAsync(string aircraftType);
        Task<IEnumerable<ParkingSpace>> AllSpacesAsync(BookingFilter filter);
        Task<bool> TryBookAsync(Guid spaceId, string aircraftType);
        Task<bool> TryRemoveBookingAsync(Guid spaceId);
    }
}
