﻿using AirportParking;
using AirportParking.Server;
using System;

public class ParkingSpaceDBModel
{
    public Guid Id { get; set; } = Guid.NewGuid();

    public ParkingSpaceSize Size { get; set; }

    public string? Name { get; set; }

    public string? Location { get; set; }

    public string? OccupiedByAircraftType { get; set; } = null;
}

