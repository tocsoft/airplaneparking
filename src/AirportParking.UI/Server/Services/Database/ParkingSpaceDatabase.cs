﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AirportParking.Database
{
    // this is so we don't have to change upstream code to know that things like ToListAsync is a thing higher up the chain
    // ideally we would know the limitation of the persistance layer upfront.
    public interface IParkingSpaceDatabase
    {
        Task<IEnumerable<T>> QueryAircraftType<T>(Func<IQueryable<AircraftType>, IQueryable<T>> queryAircraftType);
        Task<AircraftType?> SingleAircraftType(Func<AircraftType, bool> queryAircraftType);
        Task<IEnumerable<T>> QueryParkingSpaceDBModel<T>(Func<IQueryable<ParkingSpaceDBModel>, IQueryable<T>> querySpaces);
        Task<ParkingSpaceDBModel?> SingleParkingSpaceDBModel(Func<ParkingSpaceDBModel, bool> queryAircraftType);
        Task Add(ParkingSpaceDBModel space);
        Task Add(AircraftType space);

        Task Remove(ParkingSpaceDBModel space);
        Task Remove(AircraftType space);

        Task SaveAsync();
    }

    public class JsonFileParkingSpaceDatabase : IParkingSpaceDatabase
    {
        private readonly string aircraftTypesPath;
        private readonly string spacesPath;
        public string path;
        private readonly JsonSerializerOptions jsonOpts;

        public JsonFileParkingSpaceDatabase(string path)
        {
            this.aircraftTypesPath = Path.Combine(path, "aircraftTypes.json");
            this.spacesPath = Path.Combine(path, "spaces.json");
            this.path = path;

            this.jsonOpts = new JsonSerializerOptions();
            this.jsonOpts.WriteIndented = true;
            this.jsonOpts.PropertyNameCaseInsensitive = true;
            this.jsonOpts.Converters.Add(new JsonStringEnumConverter());
        }

        private List<AircraftType>? aircraftTypes = null;
        private List<ParkingSpaceDBModel>? spaces = null;

        private async Task LoadData()
        {
            // load data into memory only onces, shoudl store 2 copies and a connectino to  validate diffs before flushing individual records
            // track changes !!!
            // future optermisation would only be needed if mutliple threads/processes can access data store concurrently

            if (aircraftTypes == null)
            {
                aircraftTypes = await LoadDataAsync<AircraftType>(this.aircraftTypesPath);
            }

            if (spaces == null)
            {
                spaces = await LoadDataAsync<ParkingSpaceDBModel>(this.spacesPath);
            }
        }

        private async Task<List<T>> LoadDataAsync<T>(string path)
        {
            List<T>? data = null;

            if (File.Exists(path))
            {

                var json = await File.ReadAllBytesAsync(path);
                data = System.Text.Json.JsonSerializer.Deserialize<List<T>>(json, this.jsonOpts);
            }

            return data ?? new List<T>();
        }

        public async Task<IEnumerable<T>> QueryAircraftType<T>(Func<IQueryable<AircraftType>, IQueryable<T>> queryAircraftType)
        {
            await LoadData();

            return queryAircraftType?.Invoke(aircraftTypes.AsQueryable()).ToArray()
                ?? Array.Empty<T>();
        }

        public async Task<AircraftType?> SingleAircraftType(Func<AircraftType, bool> queryAircraftType)
        {
            await LoadData();

            return aircraftTypes.SingleOrDefault(queryAircraftType);
        }

        public async Task<ParkingSpaceDBModel?> SingleParkingSpaceDBModel(Func<ParkingSpaceDBModel, bool> queryAircraftType)
        {
            await LoadData();

            return spaces.SingleOrDefault(queryAircraftType);
        }

        public async Task<IEnumerable<T>> QueryParkingSpaceDBModel<T>(Func<IQueryable<ParkingSpaceDBModel>, IQueryable<T>> querySpaces)
        {
            await LoadData();

            return querySpaces?.Invoke(spaces.AsQueryable()).ToArray()
                ?? Array.Empty<T>();
        }

        public async Task SaveAsync()
        {
            // flush all the aircraft and spaces data back to disk 
            // (should check diffs to ensure we only flush changes but this is a nieve solution for testing only)

            if (aircraftTypes != null)
            {
                await SaveAsync(this.aircraftTypes, this.aircraftTypesPath);
            }

            if (spaces != null)
            {
                await SaveAsync(this.spaces, this.spacesPath);
            }
        }

        public async Task SaveAsync<T>(List<T> data, string path)
        {
            using (var fs = File.Create(path))
            {
                await System.Text.Json.JsonSerializer.SerializeAsync(fs, data, this.jsonOpts);
            }
        }

        public async Task Add(ParkingSpaceDBModel space)
        {
            await LoadData();
            this.spaces?.Add(space);
        }

        public async Task Add(AircraftType space)
        {
            await LoadData();
            this.aircraftTypes?.Add(space);
        }

        public async Task Remove(ParkingSpaceDBModel space)
        {
            await LoadData();
            this.spaces?.Remove(space);
        }

        public async Task Remove(AircraftType space)
        {
            await LoadData();
            this.aircraftTypes?.Remove(space);
        }
    }
}

