﻿using AirportParking;
using AirportParking.Server;

public class AircraftType
{
    public string? Reference { get; set; }

    public ParkingSpaceSize EligableSizes { get; set; }
}

