﻿using AirportParking.Database;
using AirportParking.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirportParking.Server
{
    public class ParkingSpaceRepository : IParkingSpaceRepository
    {
        private IParkingSpaceDatabase db;

        public ParkingSpaceRepository(IParkingSpaceDatabase db)
        {
            this.db = db;
        }

        public async Task<bool> TryRemoveBookingAsync(Guid spaceId)
        {
            var space = await db.SingleParkingSpaceDBModel(c => c.Id == spaceId);
            if (space == null)
            {
                throw new ArgumentException($"unable to find space with id '{spaceId}'", nameof(spaceId));
            }
            if (space.OccupiedByAircraftType == null)
            {
                //not occupied
                return false;
            }
            space.OccupiedByAircraftType = null;
            await db.SaveAsync();
            return true;
        }

        public async Task<bool> TryBookAsync(Guid spaceId, string aircraftType)
        {
            var aircraft = await db.SingleAircraftType(c => c.Reference == aircraftType);
            if (aircraft == null)
            {
                throw new ArgumentException($"'{aircraftType}' is an unsupported aircraft type", nameof(aircraftType));
            }

            var space = await db.SingleParkingSpaceDBModel(c => c.Id == spaceId);
            if (space == null)
            {
                throw new ArgumentException($"unable to find space with id '{spaceId}'", nameof(spaceId));
            }

            if (space.OccupiedByAircraftType != null)
            {
                //allready occupied
                return false;
            }

            if (!aircraft.EligableSizes.HasFlag(space.Size))
            {
                // not able to fit in spot
                return false;
            }

            space.OccupiedByAircraftType = aircraftType;

            await db.SaveAsync();
            return true;
        }

        public Task<IEnumerable<string>> GetAircraftTypesAsync()
        {
            return db.QueryAircraftType(c => c.Select(d => d.Reference!));
        }

        public async Task<IEnumerable<ParkingSpaceSizeCapacity>> GetAvailibilitySummaryAsync()
        {
            var dbData = await db.QueryParkingSpaceDBModel(s => s.GroupBy(X => X.Size)
               .Select(x => new
               {
                   Size = x.Key,
                   occupied = x.Where(c => c.OccupiedByAircraftType != null).Count(),
                   total = x.Count(),
               }));

            return dbData.Select(x => new ParkingSpaceSizeCapacity(x.Size.ToString(), x.occupied, x.total))
                .ToArray();  // use array here as we want the result immutable(ish) anyway and list uses array internally anyway.
        }
        public async Task<ParkingSpaceAvailibility> GetAvailibilityAsync(string aircraftType)
        {
            var aircraft = await db.SingleAircraftType(c => c.Reference == aircraftType);
            if (aircraft == null)
            {
                throw new ArgumentException($"'{aircraftType}' is an unsupported aircraft type", nameof(aircraftType));
            }


            var all = await db.QueryParkingSpaceDBModel(c => c);

            // we store onthe air craft all the sizes it can fit in and see if the spaces is able to fit it.

            var compat = all.Where(x => aircraft.EligableSizes.HasFlag(x.Size))
                    .ToList();
            var otherSpaces = all.Where(c => !compat.Contains(c)).ToList();

            var availible = compat.Where(x => x.OccupiedByAircraftType == null).ToList();
            var occupied = compat.Where(c => !availible.Contains(c)).ToList();

            var avilibleVM = availible.Select(Convert).ToList();
            var foundSmallest = availible.OrderBy(x => x.Size).FirstOrDefault();
            var smallestFirst = foundSmallest == null ? null : Convert(foundSmallest);

            return new ParkingSpaceAvailibility
            {
                HasEligableSpaces = avilibleVM.Any(),
                BestSpace = smallestFirst,
                AvailibleSpaces = avilibleVM,
                IncompatableSpaces = otherSpaces.Select(Convert).ToList(),
                BookedSpaces = occupied.Select(Convert).ToList(),
            };
        }
        private ParkingSpace Convert(ParkingSpaceDBModel model)
        {

            return new ParkingSpace
            {
                Id = model.Id,
                Name = model.Name ?? "",
                Location = model.Location ?? "",
                Size = model.Size.ToString(),
                IsBooked = model.OccupiedByAircraftType != null
            };
        }

        public async Task<IEnumerable<ParkingSpace>> AllSpacesAsync(BookingFilter filter)
        {
            var all = await db.QueryParkingSpaceDBModel(c =>
            {
                if (filter.IncludeBooked && !filter.IncludeFree)
                {
                    c = c.Where(x => x.OccupiedByAircraftType != null);
                }
                else if (!filter.IncludeBooked && filter.IncludeFree)
                {
                    c = c.Where(x => x.OccupiedByAircraftType == null);
                }
                else if (!filter.IncludeBooked && !filter.IncludeFree)
                {
                    // jsut for completness sake not an option you woudl really want to use
                    c = c.Where(x => false);
                }
                return c;
            });

            return all.Select(Convert).ToArray();
        }
    }
}
