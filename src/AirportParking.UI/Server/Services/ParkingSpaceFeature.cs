﻿using System;

namespace AirportParking.Server
{
    [Flags]
    public enum ParkingSpaceSize
    {
        Prop  = 0b00000001,
        Jet   = 0b00000010,
        Jumbo = 0b00000100
    }
}
