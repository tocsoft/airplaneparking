﻿using System;

namespace AirportParking.Shared
{
    public class BookingRequest
    {
        public string AircraftType { get; set; } = "";
        public Guid SpaceId { get; set; }
    }
}
