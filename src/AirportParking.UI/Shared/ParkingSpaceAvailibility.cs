﻿using System.Collections.Generic;
using System.Linq;

namespace AirportParking.Shared
{
    public class ParkingSpaceAvailibility
    {
        public bool HasEligableSpaces { get; set; }

        /// <summary>
        /// The single best choice for space and the one the system recomends being used.
        /// </summary>
        public ParkingSpace? BestSpace { get; set; }

        /// <summary>
        /// All compatable and empty spaces that can be used.
        /// </summary>
        public IEnumerable<ParkingSpace> AvailibleSpaces { get; set; } = Enumerable.Empty<ParkingSpace>();

        /// <summary>
        /// All compatable but ready booked
        /// </summary>
        public IEnumerable<ParkingSpace> BookedSpaces { get; set; } = Enumerable.Empty<ParkingSpace>();

        /// <summary>
        /// Spaces that are just not compatable with this plane, i.e. all the jet places while trying to park a jumb.
        /// </summary>
        public IEnumerable<ParkingSpace> IncompatableSpaces { get; set; } = Enumerable.Empty<ParkingSpace>();
    }
}
