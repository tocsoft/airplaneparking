﻿using System;

namespace AirportParking.Shared
{
    public class RemoveBookingRequest
    {
        public Guid SpaceId { get; set; }
    }
}
