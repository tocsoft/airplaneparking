﻿using System;

namespace AirportParking.Shared
{
    public class ParkingSpaceSizeCapacity
    {
        public ParkingSpaceSizeCapacity(string size, int booked, int total)
        {
            if (booked > total) throw new ArgumentException($"{nameof(booked)} must be less than or equal to {nameof(total)}", nameof(booked));

            Size = size;
            Booked = booked;
            Availible = total - booked;
        }

        public ParkingSpaceSizeCapacity()
        { }

        public string Size { get; set; } = "";

        public int Availible { get; set; }

        public int Booked { get; set; }
    }
}
