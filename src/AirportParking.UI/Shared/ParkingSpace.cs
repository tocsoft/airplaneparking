﻿using System;

namespace AirportParking.Shared
{
    public class ParkingSpace
    {
        public Guid Id { get; set; }

        public string? Name { get; set; }

        public string? Location { get; set; }

        public string? Size { get; set; }

        public bool IsBooked { get; set; }
    }
}
