﻿using AirportParking.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirportParking.Tests.Helpers
{
    public class TestParkingSpaceDatabase : IParkingSpaceDatabase
    {
        public List<AircraftType> AircraftTypes { get; set; } = new List<AircraftType>();
        public List<ParkingSpaceDBModel> ParkingSpaces { get; set; } = new List<ParkingSpaceDBModel>();

        public Task<IEnumerable<T>> QueryAircraftType<T>(Func<IQueryable<AircraftType>, IQueryable<T>> queryAircraftType)
        {
            return Task.FromResult<IEnumerable<T>>(queryAircraftType?.Invoke(AircraftTypes.AsQueryable()).ToArray()
                ?? Array.Empty<T>());
        }

        public Task<IEnumerable<T>> QueryParkingSpaceDBModel<T>(Func<IQueryable<ParkingSpaceDBModel>, IQueryable<T>> querySpaces)
        {
            return Task.FromResult<IEnumerable<T>>(querySpaces?.Invoke(ParkingSpaces.AsQueryable()).ToArray()
                ?? Array.Empty<T>());
        }

        public bool SaveCalled { get; set; }

        public Task SaveAsync()
        {
            SaveCalled = true;
            return Task.CompletedTask;
        }

        public Task<AircraftType> SingleAircraftType(Func<AircraftType, bool> queryAircraftType)
        {
            return Task.FromResult(this.AircraftTypes.SingleOrDefault(queryAircraftType));
        }

        public Task<ParkingSpaceDBModel> SingleParkingSpaceDBModel(Func<ParkingSpaceDBModel, bool> queryAircraftType)
        {
            return Task.FromResult(this.ParkingSpaces.SingleOrDefault(queryAircraftType));
        }

        public Task Add(ParkingSpaceDBModel space)
        {
            throw new NotImplementedException();
        }

        public Task Add(AircraftType space)
        {
            throw new NotImplementedException();
        }

        public Task Remove(ParkingSpaceDBModel space)
        {
            throw new NotImplementedException();
        }

        public Task Remove(AircraftType space)
        {
            throw new NotImplementedException();
        }
    }
}
