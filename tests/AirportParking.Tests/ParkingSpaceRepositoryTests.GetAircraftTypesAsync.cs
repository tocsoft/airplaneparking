using AirportParking.Server;
using AirportParking.Tests.Helpers;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace AirportParking.Tests
{
    public partial class ParkingSpaceRepositoryTests
    {
        public class GetAircraftTypesAsync
        {
            private TestParkingSpaceDatabase DB;
            private ParkingSpaceRepository Repository;

            public GetAircraftTypesAsync()
            {
                this.DB = new TestParkingSpaceDatabase();
                this.Repository = new ParkingSpaceRepository(this.DB);
            }

            [Fact]
            public async Task AlTypesReturend()
            {
                this.DB.AircraftTypes.Add(new AircraftType
                {
                    Reference = "123"
                });
                this.DB.AircraftTypes.Add(new AircraftType
                {
                    Reference = "223"
                });
                this.DB.AircraftTypes.Add(new AircraftType
                {
                    Reference = "323"
                });
                this.DB.AircraftTypes.Add(new AircraftType
                {
                    Reference = "423"
                });
                var types = await this.Repository.GetAircraftTypesAsync();

                Assert.Equal(4, types.Count());
            }

        }
    }
}