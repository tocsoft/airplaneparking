using AirportParking.Server;
using AirportParking.Tests.Helpers;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities.ObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace AirportParking.Tests
{
    public partial class ParkingSpaceRepositoryTests
    {
        public class AllSpacesAsync
        {
            private TestParkingSpaceDatabase DB;
            private ParkingSpaceRepository Repository;

            public AllSpacesAsync()
            {
                this.DB = new TestParkingSpaceDatabase();
                this.Repository = new ParkingSpaceRepository(this.DB);

                for (var i = 0; i < 10; i++)
                {
                    this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                    {
                        OccupiedByAircraftType = null
                    });
                }
                for (var i = 0; i < 5; i++)
                {
                    this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                    {
                        OccupiedByAircraftType = "123"
                    });
                }

            }

            [Theory]
            [InlineData(true, true, 15)]
            [InlineData(true, false, 5)]
            [InlineData(false, true, 10)]
            [InlineData(false, false, 0)]
            public async Task BookingFilter(bool includeBooked, bool includeFree, int expected)
            {
                var result = await this.Repository.AllSpacesAsync(new BookingFilter
                {
                    IncludeBooked = includeBooked,
                    IncludeFree = includeFree
                });
                Assert.Equal(expected, result.Count());
            }
        }
    }
}