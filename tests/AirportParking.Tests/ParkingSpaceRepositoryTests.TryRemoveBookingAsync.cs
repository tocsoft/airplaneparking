using AirportParking.Server;
using AirportParking.Tests.Helpers;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace AirportParking.Tests
{
    public partial class ParkingSpaceRepositoryTests
    {
        public class TryRemoveBookingAsync
        {
            private TestParkingSpaceDatabase DB;
            private ParkingSpaceRepository Repository;

            public TryRemoveBookingAsync()
            {
                this.DB = new TestParkingSpaceDatabase();
                this.Repository = new ParkingSpaceRepository(this.DB);
            }

            [Fact]
            public async Task SpaceDoesNotExist()
            {
                var error = await Assert.ThrowsAsync<ArgumentException>(async () =>
                {
                    await this.Repository.TryRemoveBookingAsync(Guid.NewGuid());
                });

                Assert.Equal("spaceId", error.ParamName);
            }

            [Fact]
            public async Task NotOccupied()
            {
                var id = Guid.NewGuid();
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Id = id,
                    OccupiedByAircraftType = null
                });
                var result = await this.Repository.TryRemoveBookingAsync(id);

                Assert.False(result);
                Assert.False(this.DB.SaveCalled);
            }



            [Fact]
            public async Task OccupiedRemovedFlag()
            {
                var id = Guid.NewGuid();
                var record = new ParkingSpaceDBModel
                {
                    Id = id,
                    OccupiedByAircraftType = "abc"
                };
                this.DB.ParkingSpaces.Add(record);
                var result = await this.Repository.TryRemoveBookingAsync(id);

                Assert.True(result);
                Assert.True(this.DB.SaveCalled);
                Assert.Null(record.OccupiedByAircraftType);
            }

        }
    }
}