using AirportParking.Server;
using AirportParking.Tests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace AirportParking.Tests
{
    public partial class ParkingSpaceRepositoryTests
    {
        public class GetAvailibilityAsync
        {
            private TestParkingSpaceDatabase DB;
            private ParkingSpaceRepository Repository;

            public GetAvailibilityAsync()
            {
                this.DB = new TestParkingSpaceDatabase();
                this.Repository = new ParkingSpaceRepository(this.DB);
            }

            [Fact]
            public async Task AirecraftDoesNotExist()
            {
                var error = await Assert.ThrowsAsync<ArgumentException>(async () =>
                {
                    await this.Repository.GetAvailibilityAsync("123");
                });

                Assert.Equal("aircraftType", error.ParamName);
            }


            [Fact]
            public async Task CorrectRecordsReturned()
            {
                this.DB.AircraftTypes.Add(new AircraftType
                {
                    Reference = "123",
                    EligableSizes = ParkingSpaceSize.Prop | ParkingSpaceSize.Jet
                });
                List<ParkingSpaceDBModel> incompatableSpaces = new List<ParkingSpaceDBModel>();
                List<ParkingSpaceDBModel> booked = new List<ParkingSpaceDBModel>();
                List<ParkingSpaceDBModel> availible = new List<ParkingSpaceDBModel>();

                ParkingSpaceDBModel best = new ParkingSpaceDBModel { Size = ParkingSpaceSize.Prop }; // elligable and smallest
                availible.Add(best);
                // inelligable sapces - prop occupied
                for (var i = 0; i < 2; i++)
                {
                    booked.Add(new ParkingSpaceDBModel { Size = ParkingSpaceSize.Prop, OccupiedByAircraftType = "1234" });
                }

                // elligable sapces - jet
                for (var i = 0; i < 2; i++)
                {
                    availible.Add(new ParkingSpaceDBModel { Size = ParkingSpaceSize.Jet });
                }

                // elligable sapces - jet
                for (var i = 0; i < 5; i++)
                {
                    booked.Add(new ParkingSpaceDBModel { Size = ParkingSpaceSize.Jet, OccupiedByAircraftType = "1234" });
                }

                // ineligable sapces - jumbo
                for (var i = 0; i < 5; i++)
                {
                    incompatableSpaces.Add(new ParkingSpaceDBModel { Size = ParkingSpaceSize.Jumbo });
                }
                foreach (var r in incompatableSpaces) { this.DB.ParkingSpaces.Add(r); }
                foreach (var r in booked) { this.DB.ParkingSpaces.Add(r); }
                foreach (var r in availible) { this.DB.ParkingSpaces.Add(r); }


                // execute
                var result = await this.Repository.GetAvailibilityAsync("123");


                // verify
                Assert.Equal(best.Id, result.BestSpace.Id);
                Assert.True(result.HasEligableSpaces);
                Assert.Equal(availible.Select(s => s.Id).OrderBy(x => x).ToArray(), result.AvailibleSpaces.Select(s => s.Id).OrderBy(x => x).ToArray());
                Assert.Equal(booked.Select(s => s.Id).ToArray(), result.BookedSpaces.Select(s => s.Id).ToArray());
                Assert.Equal(incompatableSpaces.Select(s => s.Id).ToArray(), result.IncompatableSpaces.Select(s => s.Id).ToArray());
            }

        }
    }
}