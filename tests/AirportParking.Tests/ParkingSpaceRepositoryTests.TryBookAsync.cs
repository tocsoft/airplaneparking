using AirportParking.Server;
using AirportParking.Tests.Helpers;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace AirportParking.Tests
{
    public partial class ParkingSpaceRepositoryTests
    {
        public class TryBookAsync
        {
            private TestParkingSpaceDatabase DB;
            private ParkingSpaceRepository Repository;

            public TryBookAsync()
            {
                this.DB = new TestParkingSpaceDatabase();
                this.Repository = new ParkingSpaceRepository(this.DB);
            }

            [Fact]
            public async Task SpaceDoesNotExist()
            {
                var id = Guid.NewGuid();
                this.DB.AircraftTypes.Add(new AircraftType
                {
                    Reference = "123"
                });
                var error = await Assert.ThrowsAsync<ArgumentException>(async () =>
                {
                    await this.Repository.TryBookAsync(id, "123");
                });

                Assert.Equal("spaceId", error.ParamName);
            }


            [Fact]
            public async Task AirecraftDoesNotExist()
            {
                var id = Guid.NewGuid();
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Id = id,
                });
                var error = await Assert.ThrowsAsync<ArgumentException>(async () =>
                {
                    await this.Repository.TryBookAsync(id, "123");
                });

                Assert.Equal("aircraftType", error.ParamName);
            }

            [Fact]
            public async Task Occupied()
            {
                var id = Guid.NewGuid();
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Id = id,
                    OccupiedByAircraftType = "123"
                });
                this.DB.AircraftTypes.Add(new AircraftType
                {
                    Reference = "123"
                });
                var result = await this.Repository.TryBookAsync(id, "123");

                Assert.False(result);
                Assert.False(this.DB.SaveCalled);
            }


            [Fact]
            public async Task SpaceTooSmall()
            {
                var id = Guid.NewGuid();
                var space = new ParkingSpaceDBModel
                {
                    Id = id,
                    Size = ParkingSpaceSize.Jumbo,
                    OccupiedByAircraftType = null
                };
                this.DB.ParkingSpaces.Add(space);
                this.DB.AircraftTypes.Add(new AircraftType
                {
                    EligableSizes = ParkingSpaceSize.Prop,
                    Reference = "123"
                });
                var result = await this.Repository.TryBookAsync(id, "123");

                Assert.False(result);
                Assert.False(this.DB.SaveCalled);
            }


            [Fact]
            public async Task NotOccupied()
            {
                var id = Guid.NewGuid();
                var space = new ParkingSpaceDBModel
                {
                    Id = id,
                    Size = ParkingSpaceSize.Jumbo,
                    OccupiedByAircraftType = null
                };
                this.DB.ParkingSpaces.Add(space);
                this.DB.AircraftTypes.Add(new AircraftType
                {
                    EligableSizes = ParkingSpaceSize.Jumbo,
                    Reference = "123"
                });
                var result = await this.Repository.TryBookAsync(id, "123");

                Assert.True(result);
                Assert.True(this.DB.SaveCalled);
                Assert.Equal("123", space.OccupiedByAircraftType);
            }

        }
    }
}