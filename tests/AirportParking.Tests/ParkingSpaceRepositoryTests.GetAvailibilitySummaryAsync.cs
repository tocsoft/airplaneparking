using AirportParking.Server;
using AirportParking.Tests.Helpers;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace AirportParking.Tests
{
    public partial class ParkingSpaceRepositoryTests
    {
        public class GetAvailibilitySummaryAsync
        {
            private TestParkingSpaceDatabase DB;
            private ParkingSpaceRepository Repository;

            public GetAvailibilitySummaryAsync()
            {
                this.DB = new TestParkingSpaceDatabase();
                this.Repository = new ParkingSpaceRepository(this.DB);
            }


            [Fact]
            public async Task NoSpacesInDBShouldBeEmptyResultSet()
            {
                var result = await this.Repository.GetAvailibilitySummaryAsync();
                Assert.Empty(result);
            }


            [Fact]
            public async Task OnlyReturnResultsForConfiguredSizes()
            {
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Size = ParkingSpaceSize.Jumbo
                });
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Size = ParkingSpaceSize.Jumbo
                });
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Size = ParkingSpaceSize.Jumbo
                });
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Size = ParkingSpaceSize.Jet
                });

                var result = await this.Repository.GetAvailibilitySummaryAsync();
                Assert.Equal(2, result.Count());

                var jumbo = Assert.Single(result, r => r.Size == ParkingSpaceSize.Jumbo.ToString());
                var jet = Assert.Single(result, r => r.Size == ParkingSpaceSize.Jet.ToString());
            }


            [Fact]
            public async Task AvailibleCountsRecordsWhereOccupiedIsNull()
            {
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Size = ParkingSpaceSize.Jumbo
                });
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Size = ParkingSpaceSize.Jumbo
                });
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Size = ParkingSpaceSize.Jumbo,
                    OccupiedByAircraftType = "foo"
                });

                var result = await this.Repository.GetAvailibilitySummaryAsync();
                var jumbo = Assert.Single(result, r => r.Size == ParkingSpaceSize.Jumbo.ToString());
                Assert.Equal(2, jumbo.Availible);
            }


            [Fact]
            public async Task OccupiedCountsRecordsWhereOccupiedIsNotNull()
            {
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Size = ParkingSpaceSize.Jumbo
                });
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Size = ParkingSpaceSize.Jumbo
                });
                this.DB.ParkingSpaces.Add(new ParkingSpaceDBModel
                {
                    Size = ParkingSpaceSize.Jumbo,
                    OccupiedByAircraftType = "foo"
                });

                var result = await this.Repository.GetAvailibilitySummaryAsync();
                var jumbo = Assert.Single(result, r => r.Size == ParkingSpaceSize.Jumbo.ToString());
                Assert.Equal(1, jumbo.Booked);
            }
        }
    }
}